"use strict";
let tabs = document.querySelector(".tabs");
let tabsCollection = document.querySelectorAll(".tabs-title");
let tabsContent = document.querySelectorAll(".tabs-content li");

tabs.addEventListener("click", (event) => {
  tabsCollection.forEach((e) => {
    e.classList.remove("active");
  });
  event.target.classList.add("active");

  tabsContent.forEach((e) => {
    e.style.display = "none";
    if (e.dataset.content === event.target.dataset.category) {
      e.style.display = "block";
    }
  });
});
console.log(tabsContent);
