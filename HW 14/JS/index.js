"use strict";
let tabs = document.querySelector(".tabs");
let tabsCollection = document.querySelectorAll(".tabs-title");
let tabsContent = document.querySelectorAll(".tabs-content li");

tabs.addEventListener("click", (event) => {
  tabsCollection.forEach((e) => {
    e.classList.remove("active");
  });
  event.target.classList.add("active");

  tabsContent.forEach((e) => {
    e.style.display = "none";
    if (e.dataset.content === event.target.dataset.category) {
      e.style.display = "block";
    }
  });
});
console.log(tabsContent);

const changeThemeButton = document.querySelector(".change-theme-button");
changeThemeButton.addEventListener("click", () => {
  if (changeThemeButton.textContent === "Change Theme") {
    changeThemeButton.textContent = "Return Back";
    tabsCollection.forEach((e) => e.classList.add("changed-theme"));
    tabsContent.forEach((elem) => elem.classList.add("changed-theme"));
    localStorage.setItem("theme color", "Return BacK");
  } else {
    changeThemeButton.textContent = "Change Theme";
    tabsCollection.forEach((e) => e.classList.remove("changed-theme"));
    tabsContent.forEach((elem) => elem.classList.remove("changed-theme"));
    localStorage.setItem("theme color", "Change Theme");
  }
});

if (localStorage.getItem("theme color") === "Change Theme") {
  changeThemeButton.textContent = "Change Theme";
  tabsCollection.forEach((e) => e.classList.remove("changed-theme"));
  tabsContent.forEach((elem) => elem.classList.remove("changed-theme"));
} else {
  changeThemeButton.textContent = "Return Back";
  tabsCollection.forEach((e) => e.classList.add("changed-theme"));
  tabsContent.forEach((elem) => elem.classList.add("changed-theme"));
}
