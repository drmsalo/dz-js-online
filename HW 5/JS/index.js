"use strict";
// Методы объекта это действия которые можно выполнить с объектами

// Значения свойств могут иметь любой тип, включая другие объекты

// Это значение в памяти на которое можно сослаться с помощью идентификатора.

const createNewUser = () => {
  let userName = prompt("What is ur name?");
  let userLastName = prompt("What is ur Last Name?");
  const newUser = {
    firstName: userName,
    lastName: userLastName,
  };
  newUser.getLogin = () => {
    return newUser.firstName[0].toLowerCase() + newUser.lastName.toLowerCase();
  };
  console.log(newUser.getLogin());
  return newUser;
};

console.log(createNewUser());
