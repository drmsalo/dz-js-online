// DOM позволяет получить доступ к содержимому HTML
// InnerHTML - HTML содержимое в виде строки
// InnerText - позволяет задавать или получать текст элемента.
// getElementBYID, getElementBYtagname (устаревшие методы), но лучше использовать querySelector или querySelectorAll

"use strict";

const paragraphs = document.querySelectorAll("p");

paragraphs.forEach((paragraphs) => {
  paragraphs.style.color = "#ff0000";
});

console.log(paragraphs);

const optionList = document.querySelector("#optionsList");

console.log(optionList);
console.log(optionList.parentElement);
console.log(optionList.childNodes);

const newParagraph = document.querySelector("#testParagraph");

newParagraph.innerHTML = `This is a paragraph`;

const headerLi = document.querySelector(".main-header").children;

console.log(headerLi);

for (const child of headerLi) {
  child.classList.add("nav-item");
  console.log(child);
}

const sectionTitleElem = document.querySelectorAll(".section-title");
sectionTitleElem.forEach((elem) => {
  elem.classList.remove("section-title");
});
