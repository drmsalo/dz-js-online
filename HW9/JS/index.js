"use strict";

// Новый html тег можно создать через функцию (insertAdjacentHTML)
// Первый параметр функции insertAdjacentHTML отвечает за местоположенние нового об'екта,

// beforebegin - перед родительским елементом
// afterbegin - внутри родительского елемента(в начале)
// beforeend - внутри родительского елемента(в конце)
// afterend - после родительского елемента

// удалить елемент можно или через node.remove()

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

const createlist = (element, parentEl) => {
  const body = document.querySelector(".body-pg");

  element.map((elem, index) => {
    element[index] = `<li>${elem}</li>`;
  });

  if (parentEl) {
    body.insertAdjacentHTML(
      "afterbegin",
      ` ${parentEl} <ul>${element.join("")}<ul>${parentEl}`
    );
  } else {
    return body.insertAdjacentHTML(
      "afterbegin",
      `<ul>${element.join("")}</ul>`
    );
  }
};

createlist(arr, "jjj");
