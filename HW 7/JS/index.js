"use strict";
let arr = ["hello", "world", 23, "23", null, 23];

const filterBy = (arr, dataType) =>
  arr.filter((elem) => typeof elem !== dataType);
console.log(filterBy(arr, "string"));
