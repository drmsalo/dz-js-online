"use strict";

const activeImg = document.querySelectorAll(".image-to-show");
const stopContinueBtn = document.querySelector(".stop-continue-bttn");
const stopContinue = document.querySelector(".seconds-timer");

let seconds = 1;
let secondsTimer;
const secondsTimerFunc = () => {
  secondsTimer = setTimeout(function tick() {
    stopContinue.textContent = `${seconds++}s`;
    secondsTimer = setTimeout(tick, 1000);
  }, 1000);
};
secondsTimerFunc();

let index = 1;
let pictureTimer;
const pictureTimerFunc = () => {
  pictureTimer = setTimeout(function tick() {
    if (index > 3) {
      index = 0;
    }
    stopContinueBtn.style.display = "block";
    activeImg.forEach((img) => {
      img.classList.remove("active");
    });
    activeImg[index++].classList.add("active");
    pictureTimer = setTimeout(tick, 3000);
  }, 3000);
};
pictureTimerFunc();

stopContinueBtn.addEventListener("click", () => {
  if (stopContinueBtn.textContent === "Stop") {
    stopContinueBtn.textContent = "Continue";
    clearTimeout(pictureTimer);
    clearTimeout(secondsTimer);
  } else {
    stopContinueBtn.textContent = "Stop";
    pictureTimerFunc();
    secondsTimerFunc();
  }
});
