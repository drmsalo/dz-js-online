const input = document.querySelectorAll("#password, #repeat_password");

input.forEach((elem) => {
  elem.addEventListener("click", (e) => {
    if (elem.getAttribute("type") === "password") {
      elem.removeAttribute("password");
      elem.setAttribute("type", "text");
      e.target.innerHTML = `<img src="svg/eye-off.svg" alt="Show password"/>`;
    } else {
      elem.removeAttribute("type");
      elem.setAttribute("type", "password");
      e.target.innerHTML = `<img src="svg/eye.svg" alt="Show password"/>`;
    }
  });
});

const form = document.querySelector(".password-form");
const spanError = document.querySelector(".error");
spanError.style.display = "none";

form.addEventListener("submit", (e) => {
  if (input[0].value !== input[1].value) {
    spanError.classList.add("error");
    spanError.style.display = "inline-block";
    e.target.reset();
  } else {
    alert("Welcome");
    spanError.innerHTML = "";
  }
  e.preventDefault();
});
