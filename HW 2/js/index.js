"use strict";

let userName;
let userAge;
let confirmAge;

do {
  userName = prompt("Введите свое имя");
  userAge = prompt("Сколько вам лет");
} while (
  userName === "" ||
  userName === null ||
  userName === undefined ||
  isNaN(userAge) ||
  userAge === "" ||
  userAge === null
);

if (userAge < 18) {
  alert("Вы не можете посетить данный вебсайт");
} else if (userAge >= 18 && userAge <= 22) {
  confirmAge = confirm("Are you sure you want to continue?");
  if (confirmAge === true) {
    alert(`Welcome ${userName}`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else {
  alert(`Welcome ${userName}`);
}
